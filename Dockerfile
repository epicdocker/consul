FROM alpine:3.7 AS containerpilot_buildenv

#### https://github.com/joyent/containerpilot/releases/
ENV CONTAINERPILOT_VERSION="3.8.0"
ENV CONTAINERPILOT_DOWNLOAD_URL="https://github.com/joyent/containerpilot/releases/download/${CONTAINERPILOT_VERSION}/containerpilot-${CONTAINERPILOT_VERSION}.tar.gz" \
    CONTAINERPILOT_DOWNLOAD_CHECKSUM="8d8f775099d9c58ba6c80c364bda9d6e7843cc6fd531b2b3ae805f0529012ad8e5a75e92d8630afbb446d5e27e88160abe2c6dbc39ebd1608e4b825b16937575"

RUN apk --no-cache add ca-certificates \
 && wget -O /containerpilot.tar.gz ${CONTAINERPILOT_DOWNLOAD_URL} \
 && sha512sum /containerpilot.tar.gz \
 && echo "${CONTAINERPILOT_DOWNLOAD_CHECKSUM}  /containerpilot.tar.gz" | sha512sum -c \
 && mkdir -p /opt/containerpilot \
 && tar zxf /containerpilot.tar.gz -C /opt/containerpilot

#### https://hub.docker.com/_/consul/
FROM consul:1.4.1
LABEL image.name="epicsoft_consul" \
      image.description="Consul cluster for Docker Swarm" \
      maintainer="epicsoft.de" \
      maintainer.name="Alexander Schwarz <schwarz@epicsoft.de>" \
      maintainer.copyright="Copyright 2018-2019 epicsoft.de / Alexander Schwarz" \
      license="MIT"

ENV CONSUL="consul" \
    CONSUL_CONFIG_DIR="/etc/consul" \
    CONSUL_BOOTSTRAP_EXPECT="3" \
    CONSUL_DATACENTER_NAME="dc1" \
    CONTAINERPILOT="/etc/containerpilot/containerpilot.json5"

COPY [ "etc/", "etc/"]
COPY --from=containerpilot_buildenv [ "/opt/containerpilot", "/usr/local/bin" ]

RUN apk --no-cache add bash \
 && rm -rf /var/cache/apk/* \
 && chmod -R 755 /etc/epicsoft \
 && chmod +x /usr/local/bin/containerpilot

EXPOSE 8300 \
       8301 \
       8301/udp \
       8302 \
       8302/udp \
       8500 \
       8600 \
       8600/udp

ENTRYPOINT [ "/etc/epicsoft/entrypoint.sh" ]
